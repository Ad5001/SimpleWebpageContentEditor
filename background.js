/**
 * Simple local webpage content editing extension that allows in-browser edition and HTML export
 * Copyright (c) Ad5001 2023
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 **/

const TITLE_EDITING = "Simple Webpage Content Editor (editing)"
const TITLE_NOT_EDITING = "Simple Webpage Content Editor"

const exportTabHTMLToLink = () => {
    let fullHTML  = document.body.parentNode.outerHTML
    return {
        link: 'data:text/plain;charset=utf-8,' + encodeURIComponent(fullHTML),
        name: 'export-' + location.hostname + location.pathname.replaceAll('/', '-') + '.html'
    }
}

const enableEdition = () => {
    document.body.contentEditable = true
    return true
}

const disableEdition = () => {
    document.body.contentEditable = false
    return true
}

const queryEditionStatus = () => {
    return document.body.contentEditable == "true"
}

async function execute(func) {
    let tabs = await browser.tabs.query({currentWindow: true, active: true})
    
    return await browser.scripting.executeScript({
        func: func,
        target: {tabId: tabs[0].id}
    }).then((results) => results[0].result)
}

async function getEditionStatus(tabId) {
    return await browser.scripting.executeScript({
        func: queryEditionStatus,
        target: {tabId: tabId}
    }).then((results) => results[0].result)
}

/**
 * Message listener from browser scripts.
 */
function receiveMessage(message, sender, sendResponse) {
    if(!message.request) {
        console.error(`Couldn't parse message ${JSON.stringify(message)}. No 'request' field.`)
        return false // Silently error
    }
    let ret = true
    switch(message.request) {
        case 'start-editing':
            ret = execute(enableEdition)
            browser.tabs.query({currentWindow: true, active: true}).then((tabs) => {
                toggleIcon(tabs[0].id, true)
            })
            break
        case 'end-editing':
            ret = execute(disableEdition)
            browser.tabs.query({currentWindow: true, active: true}).then((tabs) => {
                toggleIcon(tabs[0].id, false)
            })
            break
        case 'export-html':
            ret = execute(exportTabHTMLToLink)
            break
        case 'query-current-tab-edition-status':
            ret = execute(queryEditionStatus)
            break
    }
    return ret
}

browser.runtime.onMessage.addListener(receiveMessage);

/**
 * Icon change listener
 */
let darkTheme = !!matchMedia('(prefers-color-scheme: dark)').matches
matchMedia('(prefers-color-scheme: dark)').addListener(({matches}) => {
    console.log("New dark theme:", !!matches)
    darkTheme = !!matches
    browser.tabs.query({}).then((tabs) => {
        for(let tab of tabs) {
            getEditionStatus(tab.id).then((editing) => toggleIcon(tab.id, editing))
        }
    })
})

async function toggleIcon(tabId, editing) {
    let title = await browser.action.getTitle({tabId: tabId})
    let icon = "icons/editor-photon"
    if(darkTheme)
        icon += "-dark"
    if(editing) {
        icon += "-editing"
        browser.action.setTitle({tabId: tabId, title: TITLE_EDITING})
    } else
        browser.action.setTitle({tabId: tabId, title: TITLE_NOT_EDITING})
    icon += ".svg"
    console.log(icon)
    browser.action.setIcon({tabId: tabId, path: icon})
}
