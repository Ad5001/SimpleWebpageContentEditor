/**
 * Simple local webpage content editing extension that allows in-browser edition and HTML export
 * Copyright (c) Ad5001 2023
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 **/

function toggleEditingUI() {
    document.body.classList.toggle("not-editing")
    document.body.classList.toggle("editing")
}

function download(link, name) {
    let element = document.createElement('a');
    element.setAttribute('href', link);
    element.setAttribute('download', name);
    console.log("Downloading", link)

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

window.addEventListener("load", () => {
    // Check for current tab information
    browser.runtime.sendMessage({
        request: "query-current-tab-edition-status"
    }).then((hasEditionStarted) => {
        if(hasEditionStarted)
            toggleEditingUI()
    });
    
    document.querySelector("#start-editing").addEventListener("click", () => {
        toggleEditingUI()
        browser.runtime.sendMessage({ request: "start-editing" });
    })
    
    document.querySelector("#save-html").addEventListener("click", () => {
        browser.runtime.sendMessage({ request: "export-html" }).then((downloadInfo) => download(downloadInfo.link, downloadInfo.name));
    })

    document.querySelector("#stop-editing").addEventListener("click", () => {
        toggleEditingUI()
        browser.runtime.sendMessage({ request: "end-editing" });
    })
})
